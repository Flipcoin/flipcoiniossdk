## FlipCoin

[![CI Status](http://img.shields.io/travis/${USER_NAME}/${REPO_NAME}.svg?style=flat)](https://travis-ci.org/${USER_NAME}/${REPO_NAME})
[![Version](https://img.shields.io/cocoapods/v/${POD_NAME}.svg?style=flat)](http://cocoapods.org/pods/${POD_NAME})
[![License](https://img.shields.io/cocoapods/l/${POD_NAME}.svg?style=flat)](http://cocoapods.org/pods/${POD_NAME})
[![Platform](https://img.shields.io/cocoapods/p/${POD_NAME}.svg?style=flat)](http://cocoapods.org/pods/${POD_NAME})

## Requirements

Flipcoin supports iOS 9.0 and above.

## Installation

FlipCoin is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

pod 'FlipCoin', '~> 1.0'

## How to use services in pod.

To use services of Flipcoin in your app, Just add a single line of code in your 'Appdelegate' file.
Here is the code sinppet.

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Call Flipcoin method.
    
    FlipCoinStaticLib *flipCoinStaticLib = [[FlipCoinStaticLib alloc] init];

    [flipCoinStaticLib registerApp];

    return YES;
}

## Author

Matt, Matt@flipcoin.mobi

## License

FlipCoin is available under the MIT license. See the LICENSE file for more info.
