#
# Be sure to run `pod lib lint ${POD_NAME}.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'FlipCoin'
s.version          = '1.0.0'
s.summary          = 'Get device fingerprints to identify the unique device.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
Get a device fingerprints to identify the unique device.
DESC

s.homepage         = 'https://Flipcoin@bitbucket.org/Flipcoin/flipcoiniossdk'

s.license          = { :type => "MIT", :file => "LICENSE" }

s.author           = { "Matt" => "Matt@flipcoin.mobi" }

s.source = { :git => "https://Flipcoin@bitbucket.org/Flipcoin/flipcoiniossdk.git",
            :tag => 'v1.0.0'}

s.ios.deployment_target = '9.0'

#s.documentation_url = ''

s.vendored_frameworks = 'Frameworks/FlipCoinStaticLib.framework'

end
