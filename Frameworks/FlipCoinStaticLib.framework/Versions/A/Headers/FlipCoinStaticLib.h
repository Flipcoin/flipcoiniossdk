//
//  FlipCoinStaticLib.h
//  FlipCoinStaticLib
//
//  Created by biz4sol-mac-1 on 10/07/17.
//  Copyright © 2017 Biz4Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FlipCoinStaticLib : NSObject

// This method is called to initiate/stop/reset the timer as per the device's activity for 30 second.
-(void)registerApp;
    
@end
